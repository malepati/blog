class CommentsController < ApplicationController
  def create
      @post = Post.find(params[:post_id])
      @comment = @post.comments.create(comment_params)
      respond_to do |format|
        format.html { redirect_to @post }
        format.js
        end
      redirect_to post_path(@post)
    end

    def destroy
       @post = Post.find(params[:post_id])
       @comment = @post.comments.find(params[:id])
       @comment.destroy
       redirect_to post_path(@post)
     end
    private
      def comment_params
        params.require(:comment).permit(:body)
      end
end
